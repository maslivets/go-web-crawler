package main

import (
    "fmt"
    "web-crawler/lib/crawler"
    "web-crawler/lib/cli"
    "strings"
    "sync"
)

func main() {

    args := cli.ParseArgs()

    var wgMain sync.WaitGroup

    fmt.Println("Crawler 9000")
    fmt.Println("Run worker:", args.Worker)
    fmt.Println("Parse depth:", args.Depth)
    fmt.Println("Urls to parse:", strings.Join(args.Urls, ", "))

    for _, siteUrl := range args.Urls {
        wgMain.Add(1)
        go func(){
            defer wgMain.Done()
            report := crawler.Crawler(siteUrl, args.Worker, args.Depth)
            cli.WriteReport(siteUrl, report)
        }()
    }

    wgMain.Wait()

    fmt.Println("Exit")
}
