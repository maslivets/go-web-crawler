package webparser

import (
    "fmt"
    "net/url"
    "net/http"
    "golang.org/x/net/html"
    "os"
)

func ParsePage(siteUrl string) (pageUrls []string) {

    reqUrl, err := url.ParseRequestURI(siteUrl)
    if err != nil {
        fmt.Fprintln(os.Stderr, "specified URL is invalid, should be [protocol://host_name/path]")
        return
    }
    if reqUrl.Scheme != "http" && reqUrl.Scheme != "https" {
        fmt.Fprintln(os.Stderr, "Wrong scheme. Allowed [http, https]")
        return
    }

    client := &http.Client{}
    client.CheckRedirect = nil

    resp, err := client.Get(siteUrl)
    if err != nil {
        fmt.Fprintln(os.Stderr, err)
        return
    }

    pageBody := resp.Body
    defer pageBody.Close()

    pageObj := html.NewTokenizer(pageBody)

    for {
        elem := pageObj.Next()

        switch elem {
        case html.ErrorToken:
            return
        case html.StartTagToken, html.SelfClosingTagToken:
            tag := pageObj.Token()
            isAnchor := tag.Data == "a"
            if !isAnchor {
                continue
            }

            uri, ok := getHref(tag)
            if !ok {
                continue
            }

            pUrl, pErr := url.Parse(uri)
            if pErr != nil {
                continue
            }

            // match hosts
            if len(pUrl.Host) > 0 && pUrl.Host != reqUrl.Host {
                continue
            }

            newUrl := reqUrl.ResolveReference(pUrl)
            pageUrls = append(pageUrls, newUrl.String())
        }
    }

    return
}

// Helper function to pull the href attribute from a Token
func getHref(t html.Token) (href string, ok bool) {
    // Iterate over all of the Token's attributes until we find an "href"
    for _, a := range t.Attr {
        if a.Key == "href" {
            href = a.Val
            ok = true
        }
    }

    return
}
