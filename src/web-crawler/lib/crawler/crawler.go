package crawler

import (
    "fmt"
    "sync"
    "web-crawler/lib/webparser"
)

// A queueUrlType implement queue channel data structure
type queueUrlType struct {
    Url      string
    PageUrls []string
    Depth    int
}

// A taskUrlType implement task channel data structure
type taskUrlType struct {
    Url   string
    Depth int
}

type URLState int

const (
    ST_NOT_VISIT URLState = 0
    ST_IN_QUEUE URLState = 1
    ST_VISIT URLState = 2
)

// A urlStatType implement all crawler states
type urlStatType struct {
    VisitedUrls map[string]URLState // 0 - not visited, 1 - sent to queue, 2 - visited
    DepthUrls   map[string]int
    ReportMap   map[string]string   // contain [page] parent_page
    Visited     int                 // already visited page count
    NotVisited  int                 // should visit page count
    mux         sync.Mutex
}

func Crawler(inputUrl string, workerCount, maxDepth int) map[string]string {

    var wg sync.WaitGroup
    var urlStat = urlStatType{
        VisitedUrls:make(map[string]URLState),
        DepthUrls:make(map[string]int),
        ReportMap:make(map[string]string),
        Visited: 0,
        NotVisited: 0,
    }

    // define channels
    taskUrlCh := make(chan taskUrlType, workerCount)
    queueUrlCh := make(chan queueUrlType)

    // run all workers
    for i := 0; i < workerCount; i++ {
        wg.Add(1)
        go workerParser(taskUrlCh, queueUrlCh, &wg, i)
    }

    // prepare first url for worker
    urlStat.NotVisited = 1
    urlStat.VisitedUrls[inputUrl] = ST_NOT_VISIT
    urlStat.DepthUrls[inputUrl] = 0
    addTask(taskUrlCh, &urlStat)

    // main Crawler loop
    Loop:
    for {
        select {
        // listen for urls which came from workers and process them
        case queueData, queueOk := <-queueUrlCh:
            if !queueOk {
                break Loop
            }

            processQueue(queueData, &urlStat, maxDepth)

            if urlStat.NotVisited > 0 && urlStat.NotVisited == urlStat.Visited {
                close(taskUrlCh)
                close(queueUrlCh)
            }

            go addTask(taskUrlCh, &urlStat)
        }
    }

    // wait til all worker are going down
    wg.Wait()

    return urlStat.ReportMap
}

// workerParser it is a worker which open url and retrieve all urls on it and put them into queue channel
func workerParser(taskUrlCh <-chan taskUrlType, queueUrlCh chan queueUrlType, wg *sync.WaitGroup, workerId int) {

    defer wg.Done()

    fmt.Println("Run worker.", workerId)

    for {
        pageUrl, ok := <-taskUrlCh
        if !ok || pageUrl.Url == "" {
            break
        }
        fmt.Println(fmt.Sprintf("Worker: [%d] Parse URL: %s", workerId, pageUrl.Url))
        // get all urls from page
        urls := webparser.ParsePage(pageUrl.Url)
        // send result to the channel
        queueUrlCh <- queueUrlType{Url: pageUrl.Url, PageUrls: urls, Depth: pageUrl.Depth}
    }

    fmt.Println("Stop worker.", workerId)

    return
}

// processQueue collect urls and put them into related structure without duplicates
// it also prepare data for output
func processQueue(queueData queueUrlType, u *urlStatType, maxDepth int) {

    u.mux.Lock()
    defer u.mux.Unlock()

    // skip already visited urls
    if u.VisitedUrls[queueData.Url] == ST_VISIT {
        fmt.Println("Duplicate", queueData.Url)
        return
    }

    // parent url register as already visited
    u.VisitedUrls[queueData.Url] = ST_VISIT
    u.DepthUrls[queueData.Url] = queueData.Depth
    // collect visited url stat
    u.Visited++

    // register new url with no visit state
    for _, qUrl := range queueData.PageUrls {

        _, okVisit := u.VisitedUrls[qUrl]
        if !okVisit && queueData.Depth < maxDepth {
            u.VisitedUrls[qUrl] = ST_NOT_VISIT
            u.DepthUrls[qUrl] = queueData.Depth + 1
            // collect url for reporting
            u.ReportMap[qUrl] = queueData.Url
            // collect not visited url stat
            u.NotVisited++
        }
    }

    fmt.Println(fmt.Sprintf("[%d / %d]", u.NotVisited, u.Visited))
}

// addTask add single url to queue for worker parser
func addTask(taskUrlCh chan taskUrlType, u *urlStatType) {

    var t []taskUrlType

    u.mux.Lock()
    // add one url to the task channel
    for newUrl, visitState := range u.VisitedUrls {
        if visitState == ST_NOT_VISIT {
            u.VisitedUrls[newUrl] = ST_IN_QUEUE
            t = append(t, taskUrlType{Url:newUrl, Depth:u.DepthUrls[newUrl]})
        }
    }
    u.mux.Unlock()

    for _, elUrl := range t {
        taskUrlCh <- elUrl
    }
}
