package cli

import (
    "fmt"
    "net/url"
    "time"
    "os"
)

func WriteReport(siteUrl string, rData map[string]string) {

    urlObj, err := url.ParseRequestURI(siteUrl)
    if err != nil {
        return
    }

    t := time.Now()

    var fileName string = "report__" + urlObj.Host + "__" + t.Format("20060102_150405") + ".csv"

    var buf string
    for page, parentPage := range rData {
        buf += fmt.Sprintf("%s,%s\r\n", page, parentPage)
    }

    f, fErr := os.Create(fileName)
    if fErr != nil {
        panic(fErr)
    }
    defer f.Close()

    f.WriteString(buf)
    f.Sync()

    fmt.Println("Write report into file:", fileName)
}
