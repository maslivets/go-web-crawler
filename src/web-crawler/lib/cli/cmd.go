package cli

import (
    "os"
    "fmt"
    "strings"
    "strconv"
)

type optType struct {
    Depth  int
    Worker int
    Urls   []string
}

const DEFAULT_DEPTH int = 1
const DEFAULT_WORKER int = 4

var optInt map[string]int = map[string]int{
    "-d": DEFAULT_DEPTH,
    "-w": DEFAULT_WORKER,
}

var params []string

func ParseArgs() optType {
    var skipNext bool = false
    args := os.Args[1:]

    if len(args) == 0 {
        printUsage()
        os.Exit(0)
    }

    var maxIdx int = len(args) - 1

    for idx, arg := range args {

        if skipNext {
            skipNext = false
            continue
        }

        if strings.Index(arg, "-") == 0 && idx < maxIdx {
            skipNext = true
            setOpt(arg, args[idx + 1])
        } else if strings.Index(arg, "-") == 0 && idx == maxIdx {
            // boolean option
            if arg == "--help" || arg == "-h" {
                printUsage()
                os.Exit(0)
            }
        } else {
            // not a flag
            setNotOpt(arg)
        }
    }

    if len(params) == 0 {
        fmt.Println("Url not specified. Nothing to parse.")
        os.Exit(0)
    }

    return optType{Depth:optInt["-d"], Worker:optInt["-w"], Urls:params}
}

func setOpt(key string, value string) {

    _, isAllowed := optInt[key]
    if !isAllowed {
        fmt.Println(fmt.Sprintf("Option `%s` is not allowed", key))
        return
    }
    // validate
    parsedVal, errParse := strconv.Atoi(value)
    if errParse != nil {
        fmt.Println("Parse error:", errParse)
        return
    }

    if parsedVal <= 0 {
        fmt.Println(fmt.Sprintf("Value for `%s` can't be less then zero [%d]", key, parsedVal))
        return
    }

    optInt[key] = parsedVal
}

func setNotOpt(param string) {
    fmt.Println(param)
    params = append(params, param)
    // @todo: validate url
}

func printUsage() {

    var usageText = "Crawler 9000!\n" +
    "Usage: crawler [<flags>] <urls>\n\n" +
    "Flags:\n" +
    "  -d {n}  Parse depth\n" +
    "  -w {n}  Amount of workers\n\n" +
    "Example:\n" +
    "  crawler -d 1 -w 4 http://golang.org"

    fmt.Println(fmt.Sprintf(usageText))
}
